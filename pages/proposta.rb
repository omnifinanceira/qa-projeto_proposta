require_relative "../support/env.rb"
require_relative "util.rb"
require_relative "omnifacil.rb"
require_relative "pagamento.rb"
require_relative "aprovar.rb"
require_relative "aprovasuper.rb"
require_relative "pagamentoavulso.rb"
require_relative "propostadados.rb"
require_relative "visualizaccb.rb"

class Proposta < Util
  def preencher_proposta
    @login = ""
    @senha = ""
    @agente = ""
    @texto = ""

    @metodos = Metodos.new

    # 12/08/2021 Incluído por Valmir Soares para código da mesa de crédito
    @sql = ""
    @sql = Queries.new

    @omnifacil = OmniFacil.new
    @pagamento = Pagamento.new
    @aprovar = Aprovar.new
    @visualizaccb = Visualizarccb.new
    @propostadados = Preencher.new
    @aprovasuper = Aprovasuper.new

    @sql2 = ""
    @dados = Gera_dados.new
    i = 1
    # 03/11/2021 Nova Implementação:
    ###@snova_usu = "P" # Indica que é usuário de Inclusão de Propostas

    # 01/11/2021 - Indica qual linha deve ser lida na planilha "Usuários" da Massa de Dados
    ii = 0 
    
    while i < @dados.ler_planilha.size
      begin
        planilha = @dados.ler_planilha
        #planilha1 = @dados.ler_planilha_usu
        #binding.pry
        # Carrega os dados das colunas da linha da planilha para as variáveis
        @login = planilha[i][0]
        @senha = planilha[i][1]
        # 01/11/2021 Nova Implementação: Pega dados de login usuário da planilha "Usuários" da Messa de Dados
        ##@login = planilha1[ii][0]
        ##@senha = planilha1[ii][1]
        @usuario = planilha[i][2]
        @grupo = planilha[i][3]
        @produto = planilha[i][4]
        @operador = planilha[i][5]
        @agente = planilha[i][6]
        @origem = planilha[i][7]
        @operacao = planilha[i][8]
        @tabela = planilha[i][9]
        @parcelas = planilha[i][10]
        @cpf = planilha[i][11]
        # pega o retorno na planilha
        @retorno = planilha[i][12]
        @indice = planilha[i][13]
        @nova_senha = planilha[i][21]
        @st_senha = planilha[i][22]

        @sql2 = Queries.new
        @veiculo = @sql2.pegar_veiculo(@produto)
        #binding.pry
        @placa = @veiculo[7]
        @uf_licenciamento = @veiculo[8]
        @uf_placa = @veiculo[9]
        @combustivel = @veiculo[10]
        $numero_proposta = ""
        @eguro_visivel = false
        $expira_senha = ""

        # ======> FLUXO DE ACESSO AO SISTEMA E CHECAGEM DE LOGIN

        # 03/11/2021 Nova Implementação:
        @snova_usu = "P" # Indica que é usuário de Inclusão de Propostas
        @metodos.acessar_url_omnifacil(@login, @senha, @nova_senha, @st_senha, i, @dados, @agente, @snova_usu, ii)
        #binding.pry
        # =======> VERIFICA SE É AGENTE OU LOJISTA
        # SE AGENTE CHAMA NOVO MENU E SELEÇÃO DE AGENTE <==========
        if @usuario == "Agente"
          @arquivo = "Ag"
          @metodos.clica_botao_novo_menu(@agente)
          @metodos.seleciona_agente(@agente)

          # 25/08/2021 por Valmir Soares
          # Chama o método que clica nos botões do Novo Menu, 
          @metodos.clica_botoes_menu(@opc_menu = " OPERACIONAL", @opc_sub_menu1 = "CRÉDITO", @opc_sub_menu2 = "Simulador")
        else
          # SE É LOJISTA CLICA DIRETO NO BOTÃO DO SIMULADOR
          @arquivo = "Lj"
          find(".menuBar").all("a", text: "Simulador")[0].click
          @metodos.preencher_cpf(@cpf, @usuario)
          @metodos.preencher_grupo(@produto, @grupo)
        end

        #=======> AQUI COMEÇA A CHAMADA DO SIMULADOR <==========
        if @usuario == "Agente"
          # Insere operação no campo da página da proposta
          @metodos.insere_operacao(@operacao)
          begin
            @metodos.modelo_veiculo(@veiculo[1], @veiculo[5], @veiculo[3], @veiculo[4], @veiculo[6], @parcelas, @retorno, @indice, @agente, @usuario, @arquivo)
          rescue
            @veiculo = @sql2.pegar_veiculo(@produto)
            @metodos.modelo_veiculo(@veiculo[1], @veiculo[5], @veiculo[3], @veiculo[4], @veiculo[6], @parcelas, @retorno, @indice, @agente, @usuario, @arquivo)
          end

          # Preenche o CPF e clica no botão Validar da página
          @metodos.preencher_cpf(@cpf, @usuario)

          # Clicar no botao proximo
          within_frame(find(:xpath, "//iframe")[:id]) do
            find("input[value='Próximo']").click
          end
        else
          begin
            @metodos.modelo_veiculo_lojista(@veiculo[1], @veiculo[5], @veiculo[3], @veiculo[4], @veiculo[6], @parcelas, @retorno, @indice, @agente, @usuario, @arquivo)
          rescue
            @veiculo = @sql2.pegar_veiculo(@produto)
            @metodos.modelo_veiculo_lojista(@veiculo[1], @veiculo[5], @veiculo[3], @veiculo[4], @veiculo[6], @parcelas, @retorno, @indice, @agente, @usuario, @arquivo)
          end
        end

        # Chama a Classe onde estão os métodos que executa o Preenchimento dos campos da proposta
        @propostadados.preencher_dados_proposta(@dados, @omnifacil, @metodos, i, @operador, @origem, @indice, @arquivo, @agente, @produto, @placa, @combustivel, @uf_placa, @uf_licenciamento)
        
        # Clica no botão Processar Travas
        if @usuario == "Agente"
          within_frame(find(:xpath, "//iframe")[:id]) do
            begin
              click_button("Processar Travas", wait: 500)
              sleep(3)
              #@metodos.aguardar_engrenagem
              accept_confirm("Os dados da proposta #{$numero_proposta} foram atualizados com sucesso")
              #page.driver.browser.switch_to.alert.dismiss
            rescue
              @seguro_visivel = @metodos.verificar_pop_up_seguro unless find(:xpath, "//div[@id='popup_message' and text()='Existem seguros marcados indevidamente, desmarcar no botão Seguros !']").visible? == false
            end
            #@metodos.aguardar_engrenagem
            #@seguro_visivel = @metodos.verificar_pop_up_seguro
          end

          @metodos.alterar_seguro(@seguro_visivel)

        else
          #Caso usuario seja Lojista
          within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[@value='Finalizar e Enviar para a Omni']").click
            sleep(3)
            #accept_confirm("Os dados da proposta #{$numero_proposta} foram atualizados com sucesso.")
          end
          @metodos.clicar_ok_alerta
        end

        # Retiramos esta linha debaixo e os testes pararam de quebrar.
        # Rodei uma massa que fai do código 131 a 142 e nenhum dos testes quebrou no ponto em que estava quebrando
        within_frame(find(:xpath, "//iframe")[:id]) do
            # Este método checa se foi exibida uma mensagem do tipo alert_content 
            @metodos.alerta_content
            @metodos.aguardar_engrenagem

            click_button("Enviar para a Mesa", wait: 100)
            # Clica no botão "OK" da msg js "Deseja enviar a proposta 31667199 para a Mesa Omni?"
            accept_confirm("Deseja enviar a proposta para mesa de crédito?")
            # Clica no botão "Cancelar" da msg js "Deseja incluir/alterar o checklist antes de enviar a proposta para a Mesa Omni?""
            dismiss_confirm("Deseja incluir/alterar o checklist antes de enviar a proposta para a Mesa Omni?")
            @texto = "9P_TelaPropostaGerada"
            tirar_foto(@indice.to_s + "_" + @arquivo + "_" + @agente.to_s + "_" + @texto + "_" + $numero_proposta)
            gravar_log(@indice.to_s, @arquivo, @agente.to_s, @texto, $numero_proposta)
            # Grava o número da proposta na planilha
            # @dados.gravar_numero_proposta(i, $numero_proposta)      

            # 03/09 Acrescentou as variáveis para tirar a foto
            #binding.pry
            @aprovar.mesa_aprovar_proposta(@dados, @omnifacil, @metodos, i, @agente, $numero_proposta, @sql, @indice, @arquivo, @nova_senha, @st_senha)

            # 25/11/2021 Incluir aqui a checagem se a proposta foi enviada para a Mesa do Supervisor
            # Se Sim chamar o método aqui
            if $super_analise == 'S'
                #binding.pry
                @aprovasuper.aprovar_supervisor(@dados, @omnifacil, @metodos, i, @agente, $numero_proposta, @sql, @indice, @arquivo, @nova_senha, @st_senha)
            end

            # 25/08/2021 Implementado o código que chama o fluxo de Liberação de Pagamento
            @pagamento.liberar_pagamento(@dados, @omnifacil, @metodos, i, @agente, $numero_proposta, @sql, @indice, @arquivo, @nova_senha, @st_senha)

            # 26/08/2021 Clica no botão Continuar para gerar a CCB
            @pagamento.gerada_ccb(i, @indice, @arquivo, @agente, @dados)

            # 22/10/2021 Implementar o código que chama a visualização da CCB
            @visualizaccb.visualiza_ccb(@dados, @omnifacil, @metodos, i, @agente, $numero_proposta, @sql, @indice, @arquivo, @nova_senha, @st_senha)

        end

        i += 1

      rescue Exception => @ex
        puts(@ex.message)
        @texto = "99_TST_Quebrou"
        # Chama o método que grava as informações de Log quando ocorrer excessão
        gravar_log(@indice, @arquivo, @agente, @texto, $numero_proposta, @ex)
        tirar_foto(@indice.to_s + "_" + @arquivo + "_" + @agente.to_s + "_" + @texto + "_" + $numero_proposta)
        @dados.gravar_numero_proposta_quebrou(i, $numero_proposta, @texto)
        i += 1
        @arquivo = ""
        @texto = ""
        $numero_proposta = ""
      end
    end
  end
end
