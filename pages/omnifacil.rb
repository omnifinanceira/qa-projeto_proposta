#require_relative 'util.rb'

#class OmniFacil < Util
class OmniFacil

    def logar_usuario(usuario = $usuarioagente,senha = $senhaagente)
        #elementinputusuario.set usuario
        find(:xpath, "//input[@id='p-nome']").set usuario
        #elementinputsenha.set senha
        find(:xpath, "//input[@id='p-senha']").set senha
        #elementconectar.click
        find(:xpath, "//button[@id='btn-conectar']").click
    end

    # Clica no botão que acessa a Mesa de Crédito - Nova
    def selecionar_menu_inicial(nome_menu)
        find(:xpath, "//*[text()='#{nome_menu}']").click
    end

    def listar_agente
        #elementhabilitarlistaagente.click
        find(:xpath, "//span[@role='combobox']").click
        #elementselecionaragente.click
        find(:xpath, "//li[text()='184 - BATISTELLA - MARINGA-PR']").click
        #elementvalidar.click
        find(:xpath, "//button[@id='bt-validar']").click
    end

    def selecionar_menu(menu = nil)
        if !menu.nil?
            find(:xpath, "//li/a[text()=' #{menu}']").click
        end
    end

    def selecionar_submenu(submenu = nil)
        if !submenu.nil?
            find(:xpath, "//ul/li/a[text()='#{submenu}']").click 
        end
    end

    def marcar_proposta_omni_mais
        within_frame(find(:xpath, "//iframe")[:id]) do
            within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[0][:name]) do
                find(:xpath, "//input[@id='chk-mostra']").click
            end
        end
    end

    def consultar_ficha(numero_proposta)
        #proposta em preenchimento
        @erro = ""
        within_frame(find(:xpath, "//iframe")[:id]) do
            within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
                begin
                    page.send_keys [:control, :end]
                    sleep(3)
                    find(:xpath, "//input[contains(@onclick,'#{numero_proposta}')]").click
                rescue Exception => ex
                    @erro = ex.message
                end
            end
        end

        return @erro
    end

    def acessar_etapa(nome_etapa)
        within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[@value='#{nome_etapa}']").click
        end
    end

    def consulta_historica_avalista
        within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[contains(@value, 'Avalista']").click
        end
    end

    def aceitar_etapa
        within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[contains(@onclick, 'APROVADA')]").click
            page.accept_alert
        end
    end

    def pegar_valor_prazo_omnifacil
        @dados_proposta_sucesso = []

        within_frame(find(:xpath, "//iframe")[:id]) do
            @dados_proposta_sucesso << find(:xpath, "//input[@id='vl_prest']")[:value]
            @dados_proposta_sucesso << find(:xpath, "//input[@id='P_PROP_NUM_PREST']")[:value]
        end

        puts "OmniFacil - Proposta: " +  $numero_proposta + " Prazo: " + @dados_proposta_sucesso[1] + ' ' + "Valor: " + @dados_proposta_sucesso[0]
        return @dados_proposta_sucesso
    end

    def clicar_gravar
        send_keys [:control, :end]
        within_frame(find(:xpath, "//iframe")[:id]) do
            while !find(:xpath, "//input[@value='Gravar']").visible?
            end

            find(:xpath, "//input[@value='Gravar']").click
            page.accept_alert
        end
    end

    def enviar_mesa_omni
        within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[@value='Enviar Mesa Omni']").click
            page.accept_alert
        end
    end

    #def preencher_checklist
    #    within_frame(find(:xpath, "//iframe")[:id]) do
    #        find(:xpath, "//input[contains(@value, 'teste')]").click
    #        all(:xpath, "//textarea[@class='texto']").each{|i| i.set("QA automação teste")}
#
    #        find(:xpath, "//input[@value='Gravar']").click
#
    #        accept_confirm("Deseja enviar a proposta #{$numero_proposta} para a Mesa Omni?")
    #        dismiss_confirm("Deseja incluir/alterar o checklist antes de enviar a proposta para a Mesa Omni?")
    #    end
    #end

    def acessar_grupo_proposta_mesa(cod_mesa_proposta)
        #pendente_aprova = cod_mesa_proposta[0][7]
        #mesa_original = cod_mesa_proposta[0][0] + cod_mesa_proposta[0][2] + cod_mesa_proposta[0][3]  
        # 29/10/2021 - Checa se a posição que indica se a proposta
        # Para acessar o if abaixo, o sistema verifica se a posição [7] do retorno da query é igual
        # a "S". Se sim significa que a proposta está pendente de aprovação na mesa do Supervisor
        if cod_mesa_proposta[0][7] == "S"
            #binding.pry
            mesa = "8" + "_" + "3"
            within_frame(find(:xpath, "//iframe")[:id]) do
                @nova_janela = window_opened_by {
                    find(:xpath, "//div[@id='fila#{mesa}']").find(:xpath, "//div[@id='shapeAnalise#{mesa}']/a").click
                }
            end
        else
            mesa = cod_mesa_proposta[0][0] + "_" + cod_mesa_proposta[0][2]
            within_frame(find(:xpath, "//iframe")[:id]) do
                @nova_janela = window_opened_by {
                    find(:xpath, "//div[@id='fila#{mesa}']").find(:xpath, "//div[@id='infoEspera#{mesa}']/a").click
                }
            end
        end
    end

    ###def acessar_grupo_proposta_mesa(cod_mesa_proposta)
    ###    mesa = cod_mesa_proposta[0][3] + "_" + cod_mesa_proposta[0][2]
    ###    binding.pry
    ###    within_frame(find(:xpath, "//iframe")[:id]) do
    ###        @nova_janela = window_opened_by {
    ###            find(:xpath, "//div[@id='fila#{mesa}']").find(:xpath, "//div[@id='infoEspera#{mesa}']/a").click
    ###        }
    ###    end
    ###end

    def pop_up_alerta_mesa
        begin
            within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
                if find(:xpath, "//div[@id='popup_container']").visible?
                    find(:xpath, "//*[@id='popup_ok']").click
                end
            end
        rescue Exception => ex
         ex.message
        end
    end

    def preencher_procedencia
        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
            all(:xpath, "//input[@name='P_PROCEDE_RES']")[1].click
            find(:xpath, "//input[@name='P_EM_NOME_RES']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_QUAL_END_RES']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_CONTATO_RES']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_RELACAO_RES']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//textarea[@name='P_INFORMACAO_RES']").set('QA TESTE AUTOMAÇÃO')

            all(:xpath, "//input[@name='P_PROCEDE_COM']")[1].click
            find(:xpath, "//input[@name='P_EM_NOME_COM']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_QUAL_END_COM']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_CONTATO_COM']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_RELACAO_COM']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//textarea[@name='P_INFORMACAO_COM']").set('QA TESTE AUTOMAÇÃO')

            all(:xpath, "//input[@name='P_PROCEDE_FAM1']")[1].click
            find(:xpath, "//input[@name='P_EM_NOME_FAM1']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_QUAL_END_FAM1']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_CONTATO_FAM1']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_RELACAO_FAM1']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//textarea[@name='P_INFORMACAO_FAM1']").set('QA TESTE AUTOMAÇÃO')

            all(:xpath, "//input[@name='P_PROCEDE_FAM2']")[1].click
            find(:xpath, "//input[@name='P_EM_NOME_FAM2']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_QUAL_END_FAM2']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_CONTATO_FAM2']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_RELACAO_FAM2']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//textarea[@name='P_INFORMACAO_FAM2']").set('QA TESTE AUTOMAÇÃO')
        end
    end

    def localizar_acessar_proposta_mesa(numero_proposta)
        find(:xpath, "//input[@type='search']").set(numero_proposta)
        find(:xpath, "//a[text()='#{numero_proposta}']").click
    end

    def pegar_valor_prazo_mesa
        @dados_proposta_sucesso = []

        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
            @dados_proposta_sucesso << find(:xpath, "//input[@id='vl_prest']")[:value]
            @dados_proposta_sucesso << find(:xpath, "//input[@id='P_PROP_NUM_PREST']")[:value]
        end

        puts "Mesa - Proposta: " +  $numero_proposta + " Prazo: " + @dados_proposta_sucesso[1] + ' ' + "Valor: " + @dados_proposta_sucesso[0]
        return @dados_proposta_sucesso
    end

    def foca_tela
        result = page.driver.browser.window_handles.last
        page.driver.browser.switch_to.window(result)
    end

    def finaliza_aprovacao(numero_proposta, indice, arquivo, agente)
        # Esta mensagem é a mesma para as duas situações 'Aprovar' ou 'Aceitar'
        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
            #@texto_confirma_proposta_aprovada = find(:xpath, "//form/center/font[@class='txtAzulBold12']").text
            find(:xpath, "//font[contains(text(),'Dados enviados com sucesso!')]")
            #find(:xpath, "//font[contains(text(),'Proposta:#{numero_proposta} - APROVADA')]")
            @texto = '10Aprov_AprovacaoProposta'
           #binding.pry
            tirar_foto(indice.to_s + '_' + arquivo + '_' + agente.to_s + '_' + @texto + '_' + numero_proposta)
            #valor = find("input[name='P_VALOR_COTACAO1']").value.delete("^0-9,").to_f
            #binding.pry
            if find(:xpath, "//font[contains(text(),'Valor da Alçada do Analista menor que o Valor da Proposta.')]").visible?
                $super_analise = 'S'
            end

            # 24/11/2021 IMPLEMENTAÇÕES A SEREM FEITAS.
            # Neste ponto, caso a proposta ainda não seja aprovada e tenha sido enviada para a 
            # fila do Supervior, será exibida a seguinte mensagem:
            # "Proposta:NumProposta - Enviada para fila de Aprovação do Supervisor."
            # Assim sendo, temos que implementar uma checagem condicional como segue:
            # 1. Checar se a proposta foi enviada para Fila Supervisor através da mensagem;
            # 2. Se sim carregar uma variável indicando esta situação;
            # 3. Sair da condicção da checagem e processar as próximas linhas, até rodar a linha
            #    "p"age.driver.browser.swuitch_to.window(page_anterior)", quando a ultima tela será fechada;
            # 4. Em seguida criar o fluxo que vai chamar o arquivo "aprovasuper.rb", onde será realizada
            #    a análise e aprovação da proposta pelo usuário Supervisor;
            # 5. Após o término deste fluxo, a automação retorna para o arquivo proposta.rb, após a linha
            #    a chamada do método "@aprovar.mesa_aprovar_proposta" onde a análise inicial da proposta foi
            #    iniciada. O fluxo então segue normalmente, chamando o método de Liberação de Pagamento.

            page_ultima = page.driver.browser.window_handles.last
            page_anterior = page.driver.browser.window_handles.first
            page.driver.browser.switch_to.window(page_ultima)
            current_window
            current_window.close

            page.driver.browser.switch_to.window(page_anterior)

            ##### ---- Comandos para Manipular duas janelas (no browser) abertas
            # Carrega a última janela (ou atual) que está aberta para uma variável
            # result = page.driver.browser.window_handles.last 
            # Usando a variável, foca na última tela das duas que foram abertas
            # page.driver.browser.switch_to.window(result)
            # Carrega a primeira janela (ou a anterior) que está aberta para uma variável
            # anterior = page.driver.browser.window_handles.first
            # Usando a variável, foca na primeira tela das duas que foram abertas     
            # page.driver.browser.switch_to.window(anterior)
        end
    end

    def finaliza_aprovacao_supervisor(numero_proposta, indice, arquivo, agente)
        # Esta mensagem é a mesma para as duas situações 'Aprovar' ou 'Aceitar'
        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
            #@texto_confirma_proposta_aprovada = find(:xpath, "//form/center/font[@class='txtAzulBold12']").text
            find(:xpath, "//font[contains(text(),'Dados enviados com sucesso!')]")
            #find(:xpath, "//font[contains(text(),'Proposta:#{numero_proposta} - APROVADA')]")
            @texto = '11Aprov_AprovacaoPropostaSupervisor'
            binding.pry
            #tirar_foto(indice.to_s + '_' + arquivo + '_' + agente.to_s + '_' + @texto + '_' + numero_proposta)
            tirar_foto(indice.to_s + '_' + arquivo.to_s + '_' + agente.to_s + '_' + @texto + '_' + numero_proposta.to_s)
            ####if find(:xpath, "//font[contains(text(),'Valor da Alçada do Analista menor que o Valor da Proposta.')]").visible?
            ####    $super_analise = 'S'
            ####end
            page_ultima = page.driver.browser.window_handles.last
            page_anterior = page.driver.browser.window_handles.first
            page.driver.browser.switch_to.window(page_ultima)
            current_window
            current_window.close

            page.driver.browser.switch_to.window(page_anterior)
        end
    end


    #new_window=page.driver.browser.window_handles.last 
    #page.within_window new_window do
    #    #code
    #end
    #page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)

    #def clicar_gravar_mesa
    def clicar_reprocessar_travas
        send_keys [:control, :end]
        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
            #while !find(:xpath, "//input[@value='Gravar']").visible?
            while !find(:xpath, "//input[@value='Reprocessar Travas']").visible?
            end
            ####binding.pry
            #find(:xpath, "//input[@value='Gravar']").click
            find(:xpath, "//input[@value='Reprocessar Travas']").click
            ####binding.pry
            page.accept_alert
        end
    end

    def aceitar_etapa_mesa
        send_keys [:control, :end]
        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
            find(:xpath, "//input[contains(@onclick, 'APROVADA')]").click
            page.accept_alert
        end
    end

    def aceitar_etapa_mesa_credito(abasmesa)
        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
            find(:xpath, "//input[@value='#{abasmesa}']").click
            send_keys [:control, :end]
            find(:xpath, "//input[@value='Aceitar']").click
            page.accept_alert
        end
    end

    def clica_botao_decisao(abasmesa)
        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
            find(:xpath, "//input[@value='#{abasmesa}']").click
        end
    end

    def clica_botao_aprovar_decisao    
        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
           begin
              find(:xpath, "//input[@value='Aprovar']").click
           rescue
              find(:xpath, "//input[@value='Aceitar']").click
           end
           accept_confirm("Deseja finalizar a proposta com status APROVADA?")            
        end
    end

    #def preencher_observacao_decisao
    #    within_frame(find(:xpath, "//iframe")[:id]) do
    #        find(:xpath, "//input[contains(@value, 'teste')]").click
    #        all(:xpath, "//textarea[@class='texto']").each{|i| i.set("QA automação teste")}
    #        find(:xpath, "//input[@value='Gravar']").click
    #        accept_confirm("Deseja enviar a proposta #{$numero_proposta} para a Mesa Omni?")
    #        dismiss_confirm("Deseja incluir/alterar o checklist antes de enviar a proposta para a Mesa Omni?")
    #    end
    #end

    def preencher_check_list
        within_window @nova_janela do
            within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
                #if has_xpath?("//input[@value='#008_COMPROVANTE DE RENDA']") == true
                #    find(:xpath, "//input[@value='#008_COMPROVANTE DE RENDA']").click
                # end
                check_list_cp_renda
                check_list_crv
                #find(:xpath, "//input[@value='#008_COMPROVANTE DE RENDA']").click
                #find(:xpath, "//input[@value='#012_CRV - CERTIFICADO DE REGISTRO DO VEÍCULO']").click
                find(:xpath, "//textarea[@name='p_parecer']").set('Teste QA Omnifacil')
                find(:xpath, "//input[@value='Validar']").click
                accept_confirm('Deseja finalizar a proposta?')
    
                #find(:xpath, "//font[@class='txtAzulBold12']).text == "Dados enviados com sucesso!\nProposta:#{$numero_proposta} - Enviada para fila de Aprovação do Supervisor."
                # 19/08/2021 Retirada a linha abaixo
                #find(:xpath, "//font[@class='txtAzulBold12'][contains(text(),'Dados enviados com sucesso!')]")
            end
        end
    end

    def check_list_cp_renda
        begin
          if find(:xpath, "//input[@value='#008_COMPROVANTE DE RENDA']").visible?
             if find(:xpath, "//input[@value='#008_COMPROVANTE DE RENDA']").check
                find(:xpath, "//input[@value='#008_COMPROVANTE DE RENDA']").click
             end
          end
        rescue
        #
        end
    end

    def check_list_crv
        begin
          binding.pry
          if find(:xpath, "//input[@value='#012_CRV - CERTIFICADO DE REGISTRO DO VEÍCULO']").visible?
             if find(:xpath, "//input[@value='#012_CRV - CERTIFICADO DE REGISTRO DO VEÍCULO']").check
                find(:xpath, "//input[@value='#012_CRV - CERTIFICADO DE REGISTRO DO VEÍCULO']").click
             end
          end
        rescue
        #
        end
    end

    def converter_valor(valor_str)

        vlr_string = valor_str.gsub(".","")
        vlr_string = vlr_string.gsub(",",".")
        $vlr_convertido = vlr_string.to_f

        return $vlr_convertido
    end

end