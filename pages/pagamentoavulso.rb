require_relative "../support/env.rb"
require_relative "util.rb"
require_relative "proposta.rb"
require_relative "omnifacil.rb"
require_relative "metodos.rb"
require_relative "gera_dados.rb"

class PagamentoAvulso < Util
    def liberar_pagamento_avulso
	    @login = ""
        @senha = ""
        @agente = ""
        @texto = ""
###
        @metodos = Metodos.new
        @sql = Queries.new 
        @omnifacil = OmniFacil.new
        @dados = Gera_dados.new

        # 30/11/2021 Nova Implementação:
        #   Lê uma nova planilha do excel onde constam os logins e senhas dos usuários de mesa, de 
        #   Liberação de Pagamento, Visualização de CCB, etc.
        #   ii = a 3, pega o login de usuário com perfil de Supervisor de Mesa de Crédito
        # ii = a 2, pega dados da linha 2 da planilha Massa_usuarios.xlsx - Usuário de Pagamento/Visualização
        ii = 2
        planilha_usu = @dados.ler_planilha_usu
        @login = planilha_usu[ii][0]
        @senha = planilha_usu[ii][1]

        # 30/11/2021 Implementação Mantida:
        #   Lê a planilha do excel onde consta o restante das informações de parâmetros do cenário
        #   que serão utilizados na automação da proposta.
        #   O conteúdo da variável i não será fixo, pois ele será enviado como parâmetro da chamada
        #   que vem do arquivo proposta.rb
        i = 1
        planilha = @dados.ler_planilha
        planilha_usu = @dados.ler_planilha
        # Carrega os dados das colunas da linha da planilha para as variáveis
        @st_senha = planilha[i][22]
        @agente = planilha[i][6]
        @indice = planilha[i][13]
        @numero_proposta = planilha[i][15]
		
        
        @snova_usu = "L" # Indica que é usuário de Liberação de Pagamento

        #binding.pry
		@metodos.acessar_url_omnifacil(@login, @senha, @nova_senha, @st_senha, i, @dados, @agente, @snova_usu, ii)

        @metodos.clica_botao_novo_menu(@agente)

        @metodos.seleciona_agente(@agente)
  
        # 25/08/2021 por Valmir Soares
        # Chama o método que clica nos botões do Novo Menu, 
        # neste caso, selecionando a Tela do Simulador
        @metodos.clica_botoes_menu(@opc_menu = " OPERACIONAL", @opc_sub_menu1 = "CRÉDITO", @opc_sub_menu2 = "Fila")

        @omnifacil.consultar_ficha(@numero_proposta)

        within_frame(find(:xpath, "//iframe")[:id]) do
            #find("select[name='P_FAVORECIDO']").find("option[value='3']").select_option
            find("select[name='P_FAVORECIDO']").find("option[value='1']").select_option
            #vlr_falta_liberar = find(:xpath, "//input[@id='fFaltaLiberar']").value
            @valor_str = find(:xpath, "/html/body/form[3]/table[3]/tbody/tr[2]/td[9]/input").value
            @omnifacil.converter_valor(@valor_str)
            vlr_retorno_origem = $vlr_convertido

            ###vlr_retorno_origem = vlr_retorno_origem.gsub(".",",")
            ###vlr_retorno_origem = vlr_retorno_origem.gsub(",",".")
            ###vlr_retorno = vlr_retorno_origem.to_f
            @valor_str = find(:xpath, "//input[@id='fFaltaLiberar']").value
            @omnifacil.converter_valor(@valor_str)
            vlr_falta_liberar = $vlr_convertido

            ###vlr_falta_liberar = vlr_falta_liberar.gsub(".","")
            ###vlr_falta_liberar = vlr_falta_liberar.gsub(",",".")
            ###vlr_liberar = vlr_falta_liberar.to_f
            $vlr_total_liberar = vlr_falta_liberar + vlr_retorno_origem
            binding.pry
            find(:xpath, "//input[@name='P_VALOR_FAVOREC']").set($vlr_total_liberar)
            find(:xpath, "//input[@id='botMotivo']").click

            @omnifacil.foca
            find(:xpath, "/html/body/form/table/tbody/tr[3]/td[3]/input").set($vlr_total_liberar).native.send_keys :tab
            #find(:xpath, "/html/body/form/table/tbody/tr[3]/td[3]/input").native.send_keys :tab
            find(:xpath, "//input[@value='Gravar']").click
            #end

            find(:xpath, "/html/body/form/table/tbody/tr[3]/td[3]/input").set(vlr_total_liberar)
            #vlr_total_liberar = find(:xpath, "//input[@id='fTotalLiberar']").value
            #find(:xpath, "//input[@name='P_VALOR_FAVOREC']").set(vlr_falta_liberar)
            find(:xpath, "//input[@name='P_USUARIO_LIBERA']").set(login)
            find(:xpath, "//input[@name='P_SENHA_LIBERA']").set(senha)

            #page_anterior = page.driver.browser.window_handles.first

            find(:xpath, "//input[@value='Validar']").click
            page.accept_alert
            sleep(5)          
            #page_ultima = page.driver.browser.window_handles.last
            #page_anterior = page.driver.browser.window_handles.first
            #page.driver.browser.switch_to.window(page_ultima)
            #comando = "[onclick="if (ve_motivos_ok(this.form,1,2017,0,0,0,'CDC').submit();"]"
            #page.execute_script("$(#{comando}).click()")
            #onclick="if (ve_motivos_ok(this.form,1,2017,0,0,0,'CDC')) document.form1.submit();"
        end
    end

#    def gerada_ccb(i, indice, arquivo, agente, dados)
#        within_frame(find(:xpath, "//iframe")[:id]) do
#            find(:xpath, "//input[@value='Continuar']").click
#            sleep(3)
#            @texto = '20_CCB - Contrato'
#            tirar_foto(indice.to_s + '_' + arquivo + '_' + agente.to_s + '_' + @texto)
#            # Código pega o numero do contrato da posição 0 até 15 e joga na variável
#            $numero_ccb = find(:xpath, "/html/body/form/center/font").text.gsub(/[^0-9]/,'')[0,15]
#            #$numero_contrato = $numero_ccb[0,15]
#            dados.gravar_numero_ccb(i, $numero_ccb)
#            #find(:xpath, "//input[@value='Voltar']").click
#
#            #visualiza_ccb($numero_ccb)
#        end
    #end

    #def visualiza_ccb(numero_ccb)
    #    binding.pry
    #    find(:xpath, "//li/a[text()=' OPERACIONAL']").click
    #    #find(:xpath, "//*[@id="navbar-collapse-1"]/ul[1]/li[2]/a"
    #    find(:xpath, "//ul/li/a[text()='FORMALIZAÇÃO']").click
    #    find(:xpath, "//ul/li/a[text()='Impressos']").click
    #    find(:xpath, "//ul/li/a[text()='Contratos']").click
#
    #end

end