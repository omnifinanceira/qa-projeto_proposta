require_relative 'geradorrandomico.rb'

class Util < GeradorRandomico

    def initialize
        criar_pasta_log
    end

    def criar_pasta_log
        @diretorio = "C:/report_automacao"
        Dir.mkdir(@diretorio) unless File.exists?(@diretorio)

        @diretorio = "#{Dir.pwd}/reports"
        Dir.mkdir(@diretorio) unless File.exists?(@diretorio)

        # @diretorio = "results/screenshots"
        # Dir.mkdir(@diretorio) unless File.exists?(@diretorio)

        # @diretorio = "#{Dir.pwd}/results/screenshots"
        # Dir.mkdir(@diretorio) unless File.exists?(@diretorio)
    end

    # Método que grava o número da proposta gerada, em um arquivo de texto
    def gravar(texto_a_ser_gravado)
        arquivo = "C:/report_automacao/NUMERO_PROPOSTA_#{texto_a_ser_gravado}_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.txt"
        arquivo = File.new(arquivo, "w")
        File.write(arquivo, texto_a_ser_gravado)
        arquivo.close
    end

    # Método que grava as informações de Log em um arquivo txt
    def gravar_log(indice='NA', arquivo='NA', agente='NA', texto='NA', numero_proposta='NA', excecao='NA')
        arquivo_log = "C:/report_automacao/#{indice}_#{arquivo}_#{agente}_#{texto}_#{numero_proposta}_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.txt"
        arquivo_log = File.new(arquivo_log, "w")
        File.write(arquivo_log, "#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}
            \nIndice: #{indice}
            \nArquivo: #{arquivo}
            \nAgente: #{agente}
            \nTexto: #{texto}
            \nNumero da Proposta: #{numero_proposta}
            \nExcecao: #{excecao}")

        arquivo_log.close
    end

    def pegar_horario
        horarioagora = Time.new
        return horarioagora.strftime('%H:%M')
    end
end