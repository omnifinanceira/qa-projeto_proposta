require 'cpf_cnpj'
require 'faker'
require 'ffaker'

class GeradorRandomico
    
    attr_accessor :cpf, :cnpj, :email, :nome, :cep, :numero, :nsenha

    def initialize
        gerar_cpf
        gerar_cnpj
        gerar_email
        gerar_nome
        gerar_cep
        gerar_numero
        gerar_nsenha
    end

    def gerar_cpf
        cpf = CPF.generate(true)
        return cpf
    end

    def gerar_cnpj
        cnpj = CNPJ.generate(true)
        return cnpj
    end

    def gerar_rg
        rg = FFaker::IdentificationBR.rg
        return rg
    end

    def gerar_email
        # email = Faker::Internet.email
        email = FFaker::Internet.free_email
        return email
    end

    def gerar_nome
        nome = Faker::Name.name
        return 'Teste ' + nome
    end

    def gerar_cep
        cep = '01435-001'
        return cep
    end

    def gerar_numero(inicial = 1,final = 99999)
        numero = rand(inicial..final)
        numeros = numero.to_s
        numeros.size < 2 ? numeros = "0" + numeros : numeros
        return numeros
    end

    def gerar_telefone(telefone)
        if telefone.downcase == 'celular'
            numero = '9' + gerar_numero(1111,9999).to_s + '-' + gerar_numero(1111,9999).to_s
        else
            numero = gerar_numero(1111,9999).to_s + '-' + gerar_numero(1111,9999).to_s
        end
        return numero
    end

    def gerar_nsenha
        $nsenha = 8.times.map { [*'0'..'9', *'A'..'Z'].sample }.join
        return $nsenha
    end

    ####def gerar_senhanova(s)
    ####    s1 = s.to_i
    ####    $senhanova = s1.times.map { [*'0'..'9', *'A'..'Z'].sample }.join
    ####    return $senhanova
    ####end

    #def gerar_nsenha(agente)
    #    $nsenha = 'N@' + "#{agente}" + gerar_numero(1111,9999).to_s
    #    return $nsenha
    #end

    # dia = gerar_numero(1,28)
    # mes = gerar_numero(1,12)
    # ano = gerar_numero(1990,2004)
    # dia + mes + ano
end