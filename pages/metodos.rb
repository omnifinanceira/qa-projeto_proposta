require "capybara/dsl"
require "oci8"
require "ffaker"
require_relative "../support/DAO/SQLConnect.rb"
require_relative "../support/DAO/queries.rb"
require_relative "util.rb"
require_relative "../support/helper.rb"

class Metodos < Util

  def acessar_url_omnifacil(login, senha, nova_senha, st_senha, i, dados, agente, snova_usu, ii)
    #visit "https://hml-omnifacil2.omni.com.br/hmgprojetosint/pck_login.prc_login"
    visit "https://hml-omnifacil2.omni.com.br/hml/pck_login.prc_login"
    #binding.pry
    login(login, senha, nova_senha, st_senha, i, dados, agente, snova_usu, ii)
  end

  # Executa o Login no sistema
  def login(login, senha, nova_senha, st_senha, i, dados, agente, snova_usu, ii)
    find(:xpath, "//input[@id='p-nome']").set(login)
    find(:xpath, "//input[@id='p-senha']").set(senha)
    #binding.pry
    find(:xpath, "//button[@id='btn-conectar']").click
    # 10/09/2021 Chama método que checa se a senha do usuário está para expirar
    atualiza_senha_ok(login, senha, nova_senha, st_senha, i, dados, agente, snova_usu, ii)
    #gerar_nsenha
    #puts "A Nova Senha é: #{$nsenha}"
  end

  # 10/09/2021 Método que checa se a tela que informa que a senha está para expirar é exibida
  def atualiza_senha_ok(login, senha, nova_senha, st_senha, i, dados, agente, snova_usu, ii)
    begin
      # 10/09/2021 Para Senha que está para Expirar
      # o find abaixo funciona para encontrar parte do texto do elemento
      #if find(:xpath, "//div[contains(text(), 'Falta(m)')]").visible?
      # Alert Senha: Alerta Senha Expirada ou a Expirar
      if find(:xpath, "//div/span[@class='jconfirm-title' and text()='Alerta']").visible?
         if st_senha == "S"
            click_button("OK")
            # 10/09/2021 Chama método que executa a alteração da senha do usuário
            alterar_senha(login, senha, nova_senha, st_senha, i, dados, agente, snova_usu, ii)
            # Aqui chama novamente a url após alterar a senha
            senha = $nsenha
            #binding.pry
            acessar_url_omnifacil(login, senha, nova_senha, st_senha, i, dados, agente, snova_usu, ii)
         else
            # 10/09/2021 Cancela a alteração da senha e fecha a tela
            click_button("Cancelar")
         end
      end
    rescue
#
    end
  end

  # 10/09/2021 Método que executa a alteração da senha do usuário, preenchendo os campos e clicando
  # no botão Alterar e depois no botão OK
  def alterar_senha(login, senha, nova_senha, st_senha, i, dados, agente, snova_usu, ii)
    begin
      if find(:xpath, "//div[@class='modal-body']").visible?
         # Preenche os campos da tela de Alteração de Senha
         find(:xpath, "//input[@id='usuario-altera']").set(login)
         find(:xpath, "//input[@id='senha-atual-altera']").set(senha)
         gerar_nsenha
         #puts "A Nova Senha é: #{$nsenha}"
         senha = $nsenha
         find(:xpath, "//input[@id='nova-senha-altera']").set($nsenha)
         find(:xpath, "//input[@id='confirmar-altera']").set($nsenha)
         #find(:xpath, "//input[@id='bt-gravar-altera']").click
         # Com o clique no botão "Alterar" o sistema grava a nova senha no Banco de Dados
         click_button("Alterar")
         click_button("OK")
         #dados.gravar_nova_senha_usuario(i, $nsenha, snova_usu)
         # Esse método chama o fluxo que grava a nova senha na planilha
         dados.gravar_nova_senha_usuario(i, ii, $nsenha, snova_usu)
         # Após atualizar a senha no banco e na planilha, chama novamente o link com a senha nova
         ###binding.pry
         ####acessar_url_omnifacil(login, senha, nova_senha, st_senha, i, dados, agente, snova_usu, ii)
         ###binding.pry
      end
    rescue
      #
    end
  end

  # 01/09/2021 Este método é utilizado para clicar no botão NOVO MENU quando o mesmo estiver visível.
  # Estava dentro do método seleciona_agente mas não estava funcionando corretamente
  def clica_botao_novo_menu(agente)
    begin
      if find(:xpath, "//a[@url='pck_bs.prc_menu_bs']").visible?
        find(:xpath, "//a[@url='pck_bs.prc_menu_bs']").click
      end
    rescue Exception => ex
      ex.message
    end
  end

  # 01/09/2021 Seleciona o Agente na lista que é exibida
  def seleciona_agente(agente)
    begin
      if page.has_css?("span[id='select2-p-agente-container']") == true
        find(:xpath, "//span[@id='select2-p-agente-container']").click
        all(:xpath, "//ul/li[contains(text(),'#{agente}')]")[0].click
        find(:xpath, "//button[@id='bt-validar']").click
      end
    rescue Exception => ex
      ex.message
    end
  end

  # 25/08/2021 Método que clica nas opções do Menu, dependendo de onte se encontra o sistema
  def clica_botoes_menu(opc_menu, opc_sub_menu1, opc_sub_menu2)
      find(:xpath, "//li/a[text()='#{opc_menu}']").click
      find(:xpath, "//ul/li/a[text()='#{opc_sub_menu1}']").click
      find(:xpath, "//ul/li/a[text()='#{opc_sub_menu2}']").click
  end

  def clica_botoes_menu_ccb(opc_menu, opc_sub_menu1, opc_sub_menu2, opc_sub_menu3)
      find(:xpath, "//li/a[text()='#{opc_menu}']").click
      find(:xpath, "//ul/li/a[text()='#{opc_sub_menu1}']").click
      find(:xpath, "//ul/li/a[text()='#{opc_sub_menu2}']").click
      find(:xpath, "//ul/li/ul/li/a[text()='#{opc_sub_menu3}']").click
  end
  
  def insere_operacao(operacao)
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//input[@name='P_OPERACAO']").set(operacao)
      find(:xpath, "//input[@name='P_OPERACAO']").native.send_keys :tab
    end
  end

  # Simulador Agente e Operador Agente
  def modelo_veiculo(categoria, marca, ano_modelo, modelo, versao, parcelas, retorno, indice, agente, usuario, arquivo)
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//select[@name='P_CATEGORIA1']").find(:xpath, "option[@value='#{categoria}']").select_option
      find(:xpath, "//select[@name='P_MARCA1']").find(:xpath, "option[@value='#{marca}']").select_option
      find(:xpath, "//select[@name='P_ANO_MODELO1']").find(:xpath, "option[@value='#{ano_modelo}']").select_option
      find(:xpath, "//select[@name='P_MODELO1']").find(:xpath, "option[@value='#{modelo}']").select_option
      find(:xpath, "//select[@name='P_VERSAO1']").find(:xpath, "option[@value='#{versao}']").select_option
      sleep(3)
      @texto = '1S_Simulador'
      tirar_foto(indice.to_s + '_' + arquivo + '_' + agente.to_s + '_' + @texto)
      gravar_log(indice.to_s, arquivo , agente.to_s , @texto)
      valor = find("input[name='P_VALOR_COTACAO1']").value.delete("^0-9,").to_f
      fill_in("P_PROP_VLR_LIBERADO", with: ((valor * 0.10).to_i + 1) * 100)
      @valor_lib = find("input[name='P_PROP_VLR_LIBERADO']").value
      find("input[name='P_PROP_VLR_ENT']").click
      find(:xpath, "//select[@name='P_RETORNO']").find(:xpath, "option[contains(@value, '#{retorno}')]").select_option
      #find("select[name='P_RETORNO']").all("option")[3].select_option
      find("input[name='P_PROP_NUM_PREST']").set(parcelas)
      find("input[name='P_PROP_NUM_PREST']").native.send_keys :tab
      @texto = '2S_SimuladorPropNeg'
      tirar_foto(indice.to_s + '_' + arquivo + '_' + agente.to_s + '_' + @texto)
      gravar_log(indice.to_s , arquivo , agente.to_s , @texto)
      
      fechar_alerta_zap

      find("input[value='Preencher Proposta']").click
    end
  end

  def fechar_alerta_zap
    begin
      while find("#popup_container").visible?
        within("#popup_container", wait: 5) do
          click_button("OK")
        end
      end
    rescue
    end
  end


# Simulador Lojista do Agente
@retorno_lojista = ""
def modelo_veiculo_lojista(categoria, marca, ano_modelo, modelo, versao, parcelas, retorno, indice, agente, usuario, arquivo)
  within_frame(find(:xpath, "//iframe")[:id]) do
    find(:xpath, "//select[@name='P_CATEGORIA1']").find(:xpath, "option[@value='#{categoria}']").select_option
    find(:xpath, "//select[@name='P_MARCA1']").find(:xpath, "option[@value='#{marca}']").select_option
    find(:xpath, "//select[@name='P_ANO_MODELO1']").find(:xpath, "option[@value='#{ano_modelo}']").select_option
    find(:xpath, "//select[@name='P_MODELO1']").find(:xpath, "option[@value='#{modelo}']").select_option
    find(:xpath, "//select[@name='P_VERSAO1']").find(:xpath, "option[@value='#{versao}']").select_option
    sleep(3)
    tirar_foto(indice.to_s + arquivo + agente.to_s + '_1_Simulador')
    valor = find("input[name='P_VALOR_COTACAO1']").value.delete("^0-9,").to_f
    fill_in("P_PROP_VLR_LIBERADO", with: ((valor * 0.20).to_i + 1) * 100)
    @valor_lib = find("input[name='P_PROP_VLR_LIBERADO']", wait: 30).value
    find("input[name='P_PROP_VLR_LIBERADO']").send_keys(:tab)
    sleep(15)
    # verificar_tabela_prestacoes
    find(:xpath, "//select[@id='retornoLojista']").find(:xpath, "option[@value='#{retorno.gsub('.','')}']").select_option
    # if retorno.gsub('.','') != "0"
    #     verificar_atualizacao_retorno
    # end
    sleep(15)
    find(:xpath, "//input[@name='P_RAD_PREST'][@value='#{parcelas}']").click
    tirar_foto(indice.to_s + arquivo + agente.to_s + '_2_SimuladorPropNeg')
    find("input[value='Preencher Proposta']").click
  end
end

def verificar_tabela_prestacoes
  begin
      while !find(:xpath, "//div[@id='div_prest']/table/tbody/tr/td/font").visible?
      end
  rescue Exception => ex
      ex.message
  end
  sleep(5)
end

def verificar_atualizacao_retorno
  @valor_atual = all(:xpath, "//div[@id='div_prest']/table/tbody/tr/td/font").last.text
  @valor_antigo = all(:xpath, "//div[@id='div_prest']/table/tbody/tr/td/font").last.text
  begin
      while @valor_antigo == @valor_atual
        @valor_atual = all(:xpath, "//div[@id='div_prest']/table/tbody/tr/td/font").last.text
      end
  rescue Exception => ex
      ex.message
  end
end

  # Busca CPF na base da Omni, caso não seja informado na planilha Massa de Dados
  def preencher_cpf(cpf, usuario)
    sql3 = Queries.new

    within_frame(find(:xpath, "//iframe")[:id]) do
      if cpf == nil
        cpf = sql3.pegar_cpf
        find("input[name='P_CPF_CLI']").set(cpf[0])
      else
        find("input[name='P_CPF_CLI']").set(cpf)
      end

      if usuario != 'Lojista'
        find("input[name='P_BOTAO']").click
      end
    end
  end

  def preencher_grupo(produto,grupo)
    produtonome = ""
    grupotipo = ""
    if produto == 1
      produtonome = 'VEÍCULO LEVE'
    elsif produto == 4
      produtonome = 'VEÍCULO PESADO'
    else
      produtonome = 'MOTO'
    end
    grupotipo = grupo + '###' + produtonome

    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//select[@id='cboGrupo']").find(:xpath, "option[@value='#{grupotipo}']").select_option
      # find(:xpath, "//select[@id='cboGrupo']").find(:xpath, "option[@value='#{@lista_grupo.select{|i| i.include? @grupo}}']").select_option
    end
  end

  # Seleciona o Operador dentro da Proposta
  def seleciona_operador_agente(operador, origem, indice, arquivo, agente)
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//select[@name='P_PROMOTOR']").find(:xpath, "option[@value='#{operador}']").select_option
      sleep 3
      page.select "#{origem}", from: "P_AGENTE_LOJA"
      $numero_proposta = find(:xpath, "//th/font[contains(text(), 'PROPOSTA')]").text.gsub(/[^0-9]/,'')
      @texto = '3P-Operador-Origem'
      gravar_log(indice.to_s , arquivo , agente.to_s , @texto , $numero_proposta)
    end
  end

  # ---- ALTERADO POR VALMIR SOARES EM 16/06/2021 ----
  # Preenche Dados Pessoais Obrigatórios na Proposta
  ##### def preencher_dados_pessoais_obrigatorios(indice, agente, usuario, arquivo)
  def preencher_dados_pessoais_obrigatorios(indice, agente, arquivo)  
    # 1 - DADOS PESSOAIS
    within_frame(find(:xpath, "//iframe")[:id]) do
      # Pega o número da proposta assim que acessa a tela da Proposta do Cliente
      ######$numero_proposta = find(:xpath, "//th/font[contains(text(), 'PROPOSTA')]").text.gsub(/[^0-9]/,'')
      # Campo Nome
      find("input[name='P_NOME_CLI']").set(gerar_nome) if find("input[name='P_NOME_CLI']").value == ""
      @texto = '4P_PropostaNum'
      tirar_foto(indice.to_s + '_' + arquivo + '_' + agente.to_s + '_' + @texto + '_' + $numero_proposta)
      gravar_log(indice.to_s , arquivo , agente.to_s , @texto , $numero_proposta)
      # Campo RG
      find("input[name='P_RG_CLI']").set("123751547") if find("input[name='P_RG_CLI']").value == ""
      # Campo Data de Emissão (RG)
      find("input[name='P_RG_DT_CLI']").set("01012000") if find("input[name='P_RG_DT_CLI']").value == ""
      # Campo Órgão Emissor
      find("input[name='P_RG_EMISSOR_CLI']").set("SSP") if find("input[name='P_RG_EMISSOR_CLI']").value == ""
      # Select UF RG
      # ---- ALTERADO POR VALMIR SOARES EM 16/06/2021 ----
      find("select[name='P_UF_CLI']").find("option[value='SP']").select_option
      # Campo Sexo
      find("input[name='P_SEXO_CLI'][value='M']").click
      # find("input[name='P_SEXO_CLI']").find("option[value='F']").select_option
      #find(:xpath, "//input[@value='M']").click
      # Campo Data de Nascimento
      find("input[name='P_NASC_CLI']").set("01061959") if find("input[name='P_NASC_CLI']").value == ""
      # ---- ALTERADO POR VALMIR SOARES EM 16/06/2021 ----
      # Lista Estado Civil do Cliente
      find("select[name='P_EST_CIVIL_CLI']").find("option[value='1']").select_option
      # Campo Natural de
      find("input[name='P_NATURAL_CLI']").set("SANTOS") if find("input[name='P_NATURAL_CLI']").value == ""
      # ---- ALTERADO POR VALMIR SOARES EM 16/06/2021 ----
      # Select UF (Natural de)
      find("select[name='P_NATURAL_UF_CLI']").find("option[value='SP']").select_option
      # ---- ALTERADO POR VALMIR SOARES EM 16/06/2021 ----
      # Select Nacionalidade
      find("select[name='P_NACION_CLI']").find("option[value='1']").select_option
      # Campo N de Dependentes
      find("input[name='P_DEPEND_CLI']").set("0") if find("input[name='P_DEPEND_CLI']").value == ""
      # Campo Tempo de Residência
      find("input[name='P_TEMP_RESID_CLI']").set("012002") if find("input[name='P_TEMP_RESID_CLI']").value == ""
      # ---- ALTERADO POR VALMIR SOARES EM 16/06/2021 ----
      # Select Tipo de Moradia
      find("select[name='P_TIPO_MORA_CLI']").find("option[value='1']").select_option
      # Campo Valor (da Moradia)
      find("input[name='P_VLR_IMOV_CLI']").set("40000000") if find("input[name='P_VLR_IMOV_CLI']").value == ""
      # Campo DDD (Telefone Cliente)
      find("input[name='P_DDD_FONE_CLI']").set("11") if find("input[name='P_DDD_FONE_CLI']").value == ""
      # Campo Telefone (Cliente) 
      find("input[name='P_FONE_CLI']").set ""
      find("input[name='P_FONE_CLI']").set(gerar_telefone('residencial')) if find("input[name='P_FONE_CLI']").value == ""
      # ---- ALTERADO POR VALMIR SOARES EM 16/06/2021 ----
      # Select Categoria (fone Cliente)
      find("select[name='P_CATEGORIA_FONE_CLI']").find("option[value='4']").select_option
      # Campo Contato (contado do Cliente)
      find("input[name='P_CONTATO_FONE_CLI']").set(gerar_nome) if find("input[name='P_CONTATO_FONE_CLI']").value == ""
      # Campo DDD (Celular Cliente)
      find("input[name='P_DDD_CELULAR_CLI']").set("11") if find("input[name='P_DDD_CELULAR_CLI']").value == ""
      # Campo Celular (Cliente)
      find("input[name='P_CELULAR_CLI']").set ""
      find("input[name='P_CELULAR_CLI']").set(gerar_telefone('celular')) if find("input[name='P_CELULAR_CLI']").value == ""
      # Campo E-mail (Cliente)
      find("input[name='P_EMAIL_CLI']").set(gerar_email) if find("input[name='P_EMAIL_CLI']").value == ""
      # Campo Nome da Mãe
      find("input[name='P_NOME_MAE_CLI']").set(gerar_nome) if find("input[name='P_NOME_MAE_CLI']").value == ""

      # Comando que executa a limpeza do campo
      #find("input[name='P_CEP_INI_CLI']").native.clear
      #find("input[name='P_CEP_FIM_CLI']").native.clear

      # O if checa se o campo CEP está preenchido. Se Sim a variavel recebe S e o sistema não altera o
      # o conteudo do campo. Se não, variável não recebe S e preenche o campo CEP
      # Campo CEP
      find("input[name='P_CEP_INI_CLI']").set "12236" if find("input[name='P_CEP_INI_CLI']").value == ""
      find("input[name='P_CEP_FIM_CLI']").set "180" if find("input[name='P_CEP_FIM_CLI']").value == ""
      #@cep_pessoal = ""
      if find("input[name='P_CEP_INI_CLI']").value != ""
          @cep_pessoal = "S"
      else
          find("input[name='P_CEP_INI_CLI']").set("12236")
          find("input[name='P_CEP_FIM_CLI']").set("180")
          find("input[name='P_CEP_FIM_CLI']").native.send_keys :tab
          #preencher_cep_pessoa_fisica
          page_ultima = page.driver.browser.window_handles.last
          page.driver.browser.switch_to.window(page_ultima)
          #@omnifacil.foca_tela
          #find("input[name='P_NUMERO']").set("234") if find("input[name='P_NUMERO']").value == ""
          #####page_ultima = page.driver.browser.window_handles.last
          #####page.driver.browser.switch_to.window(page_ultima)
          #####find(:xpath, "//input[@value='Confirmar']").click
          @new_window = window_opened_by { find(:xpath, "/html/body/form[4]/table[7]/tbody/tr[4]/td/table/tbody/tr/td[1]/input").click }
      end

    end

    #unless @new_window.nil?
    # Se a variável for diferente de S o sistema entra no if e clica no botão da página do CEP que foi aberta
    ### Retirado o código abaixo pois resolvemos no código do cep
    if @cep_pessoal != "S"
        within_window @new_window do
         #find("input[name='P_NUM_CLI']").set("234") if find("input[name='P_NUM_CLI']").value == ""
           find(:xpath, "//input[@value='Confirmar']").click
        end
    end

    clicar_ok_alerta

    # Se a variável for diferente de S o sistema entra no if e preenche o número no conteúdo do campo, se
    # o mesmo estiver vazio.
    # Campo Número do Endereço
    ### 02/09/2021 Retirado o código abaixo pois preenchemos o número 
     if @cep_pessoal != "S"
        within_frame(find(:xpath, "//iframe")[:id]) do
          find("input[name='P_NUM_CLI']").set("234") if find("input[name='P_NUM_CLI']").value == ""
        end
    end
    
    @texto = '5P_Dados_Pessoais_Obrigatórios'
    gravar_log(indice.to_s , arquivo , agente.to_s , @texto, $numero_proposta)
  end

  #def preencher_cep_pessoa_fisica
  #    page_ultima = page.driver.browser.window_handles.last
  #    page.driver.browser.switch_to.window(page_ultima)
  #    find("input[name='P_NUMERO']").set("234") if find("input[name='P_NUMERO']").value == ""
  #    find(:xpath, "//input[@value='Confirmar']").click
#
  #    clicar_ok_alerta
  #end

  # Preenche Dados do Cônjuge Obrigatórios na Proposta
  # 4 - DADOS DO CÔNJUGE
  def preencher_dados_conjuge
    within_frame(find(:xpath, "//iframe")[:id]) do
      # Preenche Dados do Cônjuge se o Estado Civil for igual a CASADO
      if page.has_select?("P_EST_CIVIL_CLI", selected: "CASADO") == true
         preencher_conjuge
      end
    end
  end

  # ---- ALTERADO POR VALMIR SOARES EM 16/06/2021 ----
  # Preenche Dados Profissionais Obrigatórios na Proposta
  def preencher_dados_profissionais_obrigatorios(indice, agente, arquivo)
    within_frame(find(:xpath, "//iframe")[:id]) do
      # 2 - DADOS PROFISSIONAIS
      # Lista Grau de Instrução
      find("select[name='P_PROF_GRAU_INSTR']").find("option[value='2']").select_option
      # Lista Classe Profissional
      find("select[name='P_PROF_CLASSE']").find("option[value='1']").select_option
      # Prosissão
      find("input[name='P_BSC_PROF']").set("ADVOGADO") if find("input[name='P_BSC_PROF']").value == ""
      # Empresa onde trabalha
      find("input[name='P_PROF_EMP']").set("ADVOGADO") if find("input[name='P_PROF_EMP']").value == ""
      # DDD da Empresa   
      find("input[name='P_PROF_FONE_DDD']").set("11") if find("input[name='P_PROF_FONE_DDD']").value == ""
      # Telefone da Empresa    
      find("input[name='P_PROF_FONE']").set ""
      find("input[name='P_PROF_FONE']").set(gerar_telefone('residencial')) if find("input[name='P_PROF_FONE']").value == ""
      # Campo Salário (Cliente)
      find("input[name='P_PROF_SAL']").set("700000") if find("input[name='P_PROF_SAL']").value == ""
      # Campo Tempo de Serviço
      find("input[name='P_PROF_TEMP']").set("012010") if find("input[name='P_PROF_TEMP']").value == ""
 
      # Selecina o Endereço de Correspondência na Proposta
      # 3 - ENDEREÇO DE CORRESPONDÊNCIA
      # Radio Seleciona para onde vai correpondência
      all(:xpath, "//input[@name='P_CORRESP_CLI']")[0].click

      # Preenche Dados de Referências Obrigatórios na Proposta
      # 5 - REFERÊNCIAS
      # Campo Referência Familiar 1
      find("input[name='P_REF_NOM_FAMILIA']").set(gerar_nome) if find("input[name='P_REF_NOM_FAMILIA']").value == ""
      # Campo DDD (Referência Familiar 1)
      find("input[name='P_REF_DDD_FAMILIA']").set("11") if find("input[name='P_REF_DDD_FAMILIA']").value == ""
      # Campo Telefone (Referência Familiar 1)
      find("input[name='P_REF_FONE_FAMILIA']").set ""
      find("input[name='P_REF_FONE_FAMILIA']").set(gerar_telefone('celular')) if find("input[name='P_REF_FONE_FAMILIA']").value == ""

      # Campo CEP Endereço Profissional
      @cep_profissional = ""
      if find("input[name='P_PROF_CEP_INI']").value != ""
         @cep_profissional = "S"
      else
         find("input[name='P_PROF_CEP_INI']").set("12236") 
         find("input[name='P_PROF_CEP_FIM']").set("180")

         @new_window = window_opened_by { find(:xpath, "/html/body/form[4]/table[9]/tbody/tr[5]/td/table/tbody/tr/td[1]/input[3]").click }
      end
    end

    if @cep_profissional != "S"
        within_window @new_window do
          #find(:xpath, "//input[@name='P_NUMERO']").set(gerar_numero(01, 9999))
          find(:xpath, "//input[@value='Confirmar']").click
        end
    end

    # Campo Número do Endereço
    if @cep_profissional != "S"
        within_frame(find(:xpath, "//iframe")[:id]) do
          find("input[name='P_PROF_NUM']").set("555") if find("input[name='P_PROF_NUM']").value == ""
        end
    end
    @texto = '6P_Dados_Profissionais_Obrigatórios'
    gravar_log(indice.to_s , arquivo , agente.to_s , @texto , $numero_proposta)
  end

  # within_window @new_window do
  #   #find(:xpath, "//input[@name='P_NUMERO']").set(gerar_numero(01, 9999))
  #   find(:xpath, "//input[@value='Confirmar']").click
  # end

  def preencher_dados_veiculo_obrigatorios(produto, indice, agente, placa, combustivel, uf_placa, uf_licenciamento, arquivo)
    within_frame(find(:xpath, "//iframe")[:id]) do
      # 8 - VEÍCULOS
      # Campo Utilização Agropecuária
      find("input[name='P_FL_UTIL_AGROPECUARIA1'][value='N']").click if page.has_css?("input[name='P_FL_UTIL_AGROPECUARIA1'][value='N']") == true
      # Campo Placa
      find("input[name='P_PLACA1']").set(placa) if find("input[name='P_PLACA1']").value == ""
      #find("input[name='P_PLACA1']").set("ECY7272") if find("input[name='P_PLACA1']").value == ""
      # Campo Renavam
      find("input[name='P_RENAVAM1']").set("12547898745") if find("input[name='P_RENAVAM1']").value == ""
      # Campo Chassi
      find("input[name='P_CHASSI1']").set("9az787578568za58f8") if find("input[name='P_CHASSI1']").value == ""
      # Campo Cor
      find("input[name='P_COR1']").set("PRATA") if find("input[name='P_COR1']").value == ""
      # Campo Combustível
      find("select[name='P_COMBUSTIVEL1']").find("option[value='#{combustivel}']").select_option
      #find("select[name='P_COMBUSTIVEL1']").find("option[value='4']").select_option
      # Campo UF Licenciamento
      # Em 29/06/2021 Incluído por Valmir Soares
      find("select[name='P_UF_LICENCIAMENTO1']").find("option[value='#{uf_licenciamento}']").select_option
      # Campo UF Placa
      # Em 29/06/2021 Incluído por Valmir Soares
      find("select[name='P_UF_PLACA1']").find("option[value='#{uf_placa}']").select_option
      #find("select[name='P_UF_PLACA1']").find("option[value='SP']").select_option
      # Campo Vendedor (Proprietário Legal - que está no DUT)
      find("input[name='P_VENDEDOR1']").set(gerar_nome)
      @texto = '7P_PropostaVeiculo'
      tirar_foto(indice.to_s + '_' + arquivo + '_' + agente.to_s + '_' + @texto + '_' + $numero_proposta)
      gravar_log(indice.to_s , arquivo , agente.to_s , @texto , $numero_proposta)
      # Campo CPF do Vendedor
      find("input[name='P_CPF_VENDEDOR1']").set(gerar_cpf)
            
      # 9 - PROPOSTA DE NEGÓCIO
      # Campo Crédito Mais
      find("input[name='P_COMPRA_DIVIDA'][value='N']").click if page.has_css?("input[name='P_COMPRA_DIVIDA'][value='N']") == true
      sleep 2

      # Campo OBSERVAÇÕES
      find("textarea[name='P_OBSERVACAO_AGENTE']").click if page.has_css?("textarea[name='P_OBSERVACAO_AGENTE']") == true
      find("textarea[name='P_INF_ADICIONAIS']").click if page.has_css?("textarea[name='P_INF_ADICIONAIS']") == true
      #find(<input type="text" name="P_OBSERVACAO_AGENTE" value="teste"  autofocus="true"/>)
      @texto = '8P_PropostaNegocio'
      tirar_foto(indice.to_s + '_' + arquivo + '_' + agente.to_s + '_' + @texto + '_' + $numero_proposta)
      gravar_log(indice.to_s , arquivo , agente.to_s , @texto , $numero_proposta)
    end
  end

  # IMPLEMENTAÇÃO DO CEP COM BASE NO CÓDIGO DO PROJETO ANTIGO - VALMIR 15/06/2021
  def clicar_ok_alerta
    # IMPLEMENTADO EM SETEMBRO 2020 - FECHA TELA DE ERRO EXIBIDA DUAS VEZES
    # Este código fecha uma Alerta de erro que está sendo exibida após a consulta do CEP.
    begin
      within_frame(find(:xpath, "//iframe")[:id]) do
        while find("#popup_container").visible?
          within("#popup_container", wait: 5) do
            click_button("OK")
          end
        end
      end
    rescue
    end
  end

  # Fechar Alert de Seguro que é exibido em algumas propostas
  def verificar_pop_up_seguro
   @seguro_visivel = false
   begin
     if find(:xpath, "//div[@id='popup_message' and text()='Existem seguros marcados indevidamente, desmarcar no botão Seguros !']").visible?
        click_button("OK")

        @new_window = window_opened_by { find(:xpath, "/html/body/form[4]/table[33]/tbody/tr[2]/td/table/tbody/tr[5]/td[4]/input[2]").click }
        @seguro_visivel = true
     end
     return @seguro_visivel
     #@new_window = window_opened_by { find(:xpath, "/html/body/form[4]/table[33]/tbody/tr[2]/td/table/tbody/tr[5]/td[4]/input[2]").click }
   rescue
   end
  end

  # Verifica se o Alerta que indica que o CPF tem mais de uma proposta recusada em outro produto está visivel
  # fechando o alerta ao clicar no botão OK da mesma
  # MUDAMOS PARA ESTE
  def alerta_content
      begin
  #      if find(:xpath, "//div[@id='popup_content']").be_truthy
        if find(:xpath, "//div[@id='popup_message' and text()='CPF possui 1 proposta(s) recusada(s) em outro Produto. Analise com cuidado!']").visible?
           click_button("OK")
        end
      rescue
#
      end
  end
  
  # ASSIM É COMO ESTAVA
  #def alerta_content
  #  #if find(:xpath, "//div[@id='popup_message' and text()='CPF possui 1 proposta(s) recusada(s) em outro Produto. Analise com cuidado!']").visible?
  #  if find(:xpath, "//div[@id='popup_content']").visible?
  #      click_button("OK")
  #  end
  #end

  #def aguardar_engrenagem
  #  begin
  #    retries ||= 0
  #    "vem ver"
  #    puts "#{page.driver.browser.switch_to.alert.text} SIM."
  #    page.accept_alert
  #    raise "PAGINA NAO RESPONDEU" unless assert_no_selector(".main-frame-error") == true
  #  rescue
  #    p "sera?"
  #    retry if (retries += 1) < 2
  #  end
  #end

  def aguardar_engrenagem
    begin
      while find(:xpath, "//div[@id='waitDialog']").visible?
        puts 'PASSEI NO WHILE @@@@@@@##!!!!'
      end
    rescue Exception => ex
      ex.message
    end
  end

  def alterar_seguro(seguro_visivel)
    if seguro_visivel
      within_window @new_window do
        valor_total = find(:xpath, "//div[@id='divTotalSeguro']").text
        find(:xpath, "//input[contains(@value,'#{valor_total}')]").click
        find(:xpath, "//input[@value=' Voltar ']").click
      end

      within_frame(find(:xpath, "//iframe")[:id]) do
        click_button("Processar Travas")
        accept_confirm("Os dados da proposta #{$numero_proposta} foram atualizados com sucesso")
        #sleep 8
      end
    end
  end

  def processar(botao, iframe = true)
    if iframe == false
      find("input[name='P_BOTAO'][value='#{botao}']").click
      while find(:xpath, "//div[@id='waitDialog']").visible?
      end
      page.accept_alert
    else
      within_frame(find(:xpath, "//iframe")[:id]) do
        find(:xpath, "//input[@value='#{botao}']").click
        while find(:xpath, "//div[@id='waitDialog']").visible?
        end
      end
      page.accept_alert
      page.accept_alert
    end
  end

  def valida_repique
    within_frame(find(:xpath, "//iframe")[:id]) do
      if page.has_select?("P_FL_REPIQUE") == true
        page.select "NÃO", from: "P_FL_REPIQUE"
      end
    end
  end

  def clicar_enviar_para_mesa
    sleep 2
    comando = "[onclick='submete();']"
    page.execute_script("$(#{comando}).click()")
    #find(:xpath, "//table/tbody/tr/td/input[@value='Enviar para a Mesa']").click
    sleep 2
    validar_alert_js
    page.driver.browser.switch_to.alert.dismiss
  end

  def nova_tela
    result = page.driver.browser.window_handles.last
    page.driver.browser.switch_to.window(result)
  end

  def valida_alert
    begin
      retries ||= 0
      page.accept_alert
    rescue
      retry if (retries += 1) < 2
    end
  end

  def preencher_conjuge
    find("input[name='P_CONJUGE_NOME']").set(gerar_nome)
    find("input[name='P_CONJUGE_CPF']").set(gerar_cpf)
    find("input[name='P_CONJUGE_RG']").set(gerar_rg)
    page.select "SP", from: "P_CONJUGE_UF"
    find("input[name='P_CONJUGE_NASC']").set("01011990")
    page.select "BRASILEIRA", from: "P_CONJUGE_NACIONAL"
  end

  def clicar_ok
    find(:xpath, "//button[text()='OK']").click
  end
  #def acessa_mesa_proposta_aprovo_verifico
   #  @omnifacil.logar_usuario("ANTONIO_RODRIGUES","VPHMG2021")
   #  @omnifacil.selecionar_menu_inicial("MESA DE CRÉDITO - (Nova Fila) ")
   #
   #  cod_mesa_proposta  = @sql.localizar_codigo_mesa($numero_proposta.to_i)
   #  @omnifacil.acessar_grupo_proposta_mesa(cod_mesa_proposta)
   #
   #  @texto_confirma_proposta_aprovada = @omnifacil.aprovar_proposta_mesa($numero_proposta.to_i)
   #
   #  expect(("Dados enviados com sucesso!\nProposta:#{$numero_proposta} - Enviada para fila de Aprovação do Supervisor.").match(@texto_confirma_proposta_aprovada)).to be_truthy
  #end


end