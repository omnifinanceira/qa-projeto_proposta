class Gera_dados

  def ler_planilha_usu
    arquivo= Roo::Spreadsheet.open("#{Dir.pwd}/../Massa_usuarios.xlsx")
    planilha_usu = arquivo.sheet(0)
##   binding.pry
    linhas = Array.new
    tamanho = planilha_usu.count
   for ii in 1..tamanho
      linhas << planilha_usu.row(ii)
    end
    
    return linhas
  end

  def ler_planilha
    # Abre o arquivo com a planilha
    arquivo = Roo::Spreadsheet.open("#{Dir.pwd}/../Massa_dados.xlsx")
    #Pega a primeira planilha do arquivo
    #planilha_usu = arquivo.sheet(1)
    planilha = arquivo.sheet(0)
    #binding.pry
    # 01/11/2021 Lê planilha 2 do arquivo Massa de Dados
    #planilha_usu = arquivo.sheet(1)  
    # Pega todas as linhas da planilha e joga no array 'linhas'
    linhas = Array.new
    tamanho = planilha.count

    for i in 1..tamanho
      linhas << planilha.row(i)
    end
    
    return linhas
  end

  # Grava o Número da Proposta gerada na célula da Planilha de Massa do excel
  def gravar_numero_proposta(i, numero_proposta)
    arquivo = RubyXL::Parser.parse("#{Dir.pwd}/../Massa_dados.xlsx")
    planilha = arquivo[0]
    planilha.add_cell(i,15,numero_proposta)
    ####binding.pry
    arquivo.write("#{Dir.pwd}/../Massa_dados.xlsx")
    ####binding.pry
  end
 
   # Grava o Número da Proposta gerada na célula da Planilha de Massa do excel
   def gravar_numero_proposta_quebrou(i,numero_proposta,texto)
    arquivo = RubyXL::Parser.parse("#{Dir.pwd}/../Massa_dados.xlsx")
    planilha = arquivo[0]
    planilha.add_cell(i,15,numero_proposta)
    planilha.add_cell(i,16,texto)
    arquivo.write("#{Dir.pwd}/../Massa_dados.xlsx")
  end

  # 10/09/2021 Grava a nova senha do usuário na coluna da planilha
  # 01/11/2021 Nova Implementação. Pega dados do usuário da planilha 2 da Massa de Dados
  def gravar_nova_senha_usuario(i, ii, nsenha, snova_usu)
    arquivo = RubyXL::Parser.parse("#{Dir.pwd}/../Massa_usuarios.xlsx")
    # planilha = arquivo[0]
    # 01/11/2021 Nova Implementação: Manipula dados da planilha "Usuários" da Massa de Dados
    planilha_usu = arquivo[0]
    #planilha.add_cell(indice,1,nova_senha)
    if snova_usu == "P" # variável que indica que veio da Proposta
        # 03/11/2021 Fluxo Testado com usuário Proposta e está funcionando Corretamente.
        arquivo = RubyXL::Parser.parse("#{Dir.pwd}/../Massa_dados.xlsx")
        planilha = arquivo[0]
        planilha.add_cell(i,1,nsenha)
        arquivo.write("#{Dir.pwd}/../Massa_dados.xlsx")
    elsif snova_usu == "M" # variável que indica que veio da Aprovação
        # 04/11/2021 Fluxo Testado com usuáiro MESA e está funcionando Corretamente.
        planilha_usu.add_cell(ii,1,nsenha)
    elsif snova_usu == "L" # variável indica que veio da Liberação de Pagamento
      # 03/11/2021 FALTA TESTAR ESSE FLUXO. USAR O ARQUIVO aprovarsuper.rb
        planilha_usu.add_cell(ii,1,nsenha)
    elsif snova_usu == "V" # variável indica que veio da Visualização da CCB
      # 03/11/2021 FALTA TESTAR ESSE FLUXO. USAR O ARQUIVO aprovarsuper.rb
        planilha_usu.add_cell(ii,1,nsenha)
    elsif snova_usu == "S" # variável indica que veio do Supervisor
        # Testado em 03/11/2021 Funcionou Corretamente sem erro.
        planilha_usu.add_cell(ii,1,nsenha)
    end
    #planilha.add_cell(indice,16,texto)
    arquivo.write("#{Dir.pwd}/../Massa_usuarios.xlsx")
    $expira_senha = "S"
  end

  # Grava o Número da Proposta gerada na célula da Planilha de Massa do excel
  def gravar_numero_ccb(i, numero_contrato)
    arquivo = RubyXL::Parser.parse("#{Dir.pwd}/../Massa_dados.xlsx")
    planilha = arquivo[0]
    planilha.add_cell(i,23,numero_contrato)
    arquivo.write("#{Dir.pwd}/../Massa_dados.xlsx")
  end

end
