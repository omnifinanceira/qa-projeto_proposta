# 31/08/2021 Foi passado para este método como variável, as classes e métodos, como omnifacil, metodos, dados
# e sql. Assim sendo não precisamos intanciar os mesmos aqui neste arquivo.
# Então, quando os mesmos são chamados, não precisamos utilizar o @ junto de seu nome.
class Aprovar 
    def mesa_aprovar_proposta(dados, omnifacil, metodos, i, agente, numero_proposta, sql, indice, arquivo, nova_senha, st_senha)
        #binding.pry
        @cod_mesa_proposta = sql.localizar_codigo_mesa(numero_proposta.to_i)
        #planilha = dados.ler_planilha
        
        # 01/11/2021 - Nova implementação. Pegando dados de usuário da nova planilha de usuários
        # ii = a 1, pega dados da linha 1 da planilha Massa_usuarios.xlsx - Usuário Aprovação Mesa
        #binding.pry
        ii = 1
        planilha_usu = dados.ler_planilha_usu
        login = planilha_usu[ii][0]
        senha = planilha_usu[ii][1]
        #login = planilha[i][17]
        #senha = planilha[i][18]

        snova_usu = "M" # Indica que é usuário de Mesa de Crédito

        ######## --------- Fluxo para Aprovação de Proposta na Mesa ---------- #######
        #visit "https://hml-omnifacil2.omni.com.br/hmgprojetosint/pck_login.prc_login"
        #set_url "/pck_login.prc_login"
        metodos.acessar_url_omnifacil(login, senha, nova_senha, st_senha, i, dados, agente, snova_usu, ii)

        #metodos.login(login, senha)

        #logar_usuario("ANTONIO_RODRIGUES","P@NO@2120")
        #omnifacil.logar_usuario("SILVANA_MONTEIRO","OMNI2021")
        # Chama método que clica no botão que acessa a Mesa de Crédito - Nova Fila
        omnifacil.selecionar_menu_inicial("MESA DE CRÉDITO - (Nova Fila) ")   
        #
        # 26/08/2021 Busca o Código da Mesa onde está a Proposta por Valmir Soares 
        omnifacil.acessar_grupo_proposta_mesa(@cod_mesa_proposta) 
        omnifacil.foca_tela
        # 16/08/2021 Localiza a Proposta na Mesa onde ela está por Valmir Soares
        omnifacil.localizar_acessar_proposta_mesa(numero_proposta)
        sleep(3)
        #foca_tela
        # 16/08/2021 Fecha popup de Alerta exibido na tela da Proposta por Valmir Soares
        omnifacil.pop_up_alerta_mesa
        # 16/08/2021 Preenche os campos de Procedência da Proposta por Valmir Soares
        omnifacil.preencher_procedencia
        # 16/08/2021 Após preencher Procedências, clica no botão Gravar da Proposta por Valmir Soares
        #clicar_gravar_mesa
        omnifacil.clicar_reprocessar_travas
        ##accept_confirm("Os dados da proposta #{$numero_proposta} foram atualizados com sucesso.")
        ##foca_tela
        # 16/08/2021 Fecha popup de Alerta exibido na tela da Proposta por Valmir Soares
        omnifacil.pop_up_alerta_mesa
        sleep(3)
        omnifacil.foca_tela
        # 16/08/2021 Fecha popup de Alerta exibido na tela da Proposta por Valmir Soares
        omnifacil.pop_up_alerta_mesa
        # 16/08/2021 Atribuí o nome da aba da tela para validar o botão Aceitar por Valmir Soares
        @abasmesa = 'Histórica' ## aqui está o erro
        # 16/08/2021 Acessa a Tela da Histórica e clica no botão Aceitar por Valmir Soares
        omnifacil.aceitar_etapa_mesa_credito(@abasmesa)
        omnifacil.foca_tela
        # 16/08/2021 Sistema retorna para a Ficha/Verificações.Fecha popup de Alerta da Proposta por Valmir Soares
        omnifacil.pop_up_alerta_mesa
        # 16/08/2021 Estando na Ficha/Verificações clica no botão Aceitar por Valmir Soares
        omnifacil.aceitar_etapa_mesa
        # 16/08/2021 Atribúi o nome da aba da tela para validar o botão Aceitar por Valmir Soares
        @abasmesa = 'Decisão'
        # 16/08/2021 Clica no botão Decisão por Valmir Soares
        omnifacil.clica_botao_decisao(@abasmesa)
        omnifacil.pop_up_alerta_mesa
        omnifacil.foca_tela
        omnifacil.clica_botao_aprovar_decisao
        omnifacil.preencher_check_list
        omnifacil.finaliza_aprovacao(numero_proposta, indice, arquivo, agente)
        omnifacil.foca_tela
    end
end
