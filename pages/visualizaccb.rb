class Visualizarccb
    def visualiza_ccb(dados, omnifacil, metodos, i, agente, numero_proposta, sql, indice, arquivo, nova_senha, st_senha)
        # 01/11/2021 - Nova implementação. Pegando dados de usuário nova planilha de usuários
        # ii = a 2, pega dados da linha 2 da planilha Massa_usuarios.xlsx - Usuário de Pagamento/Visualização
        ii = 2
        planilha_usu = dados.ler_planilha_usu
        login = planilha_usu[ii][0]
        senha = planilha_usu[ii][1]

        #planilha = dados.ler_planilha

        #login = planilha[i][19]
        #senha = planilha[i][20]
        
        $numero_ccb = planilha[1][23]
        snova_usu = "V" # 11/01/2021 Indica que é usuário que acessa e visualiza a CCB

        ######## --------- Fluxo para Visualização da CCB ---------- ########
        #binding.pry
        metodos.acessar_url_omnifacil(login, senha, nova_senha, st_senha, i, dados, agente, snova_usu, ii)

        metodos.clica_botao_novo_menu(agente)

        metodos.seleciona_agente(agente)
  
        # 25/08/2021 por Valmir Soares
        # Chama o método que clica nos botões do Novo Menu, 
        # neste caso, selecionando a Tela do Simulador
        metodos.clica_botoes_menu_ccb(@opc_menu = " OPERACIONAL", @opc_sub_menu1 = "FORMALIZAÇÃO", @opc_sub_menu2 = "Impressos", @opc_menu3 = "Contratos")

        within_frame(find(:xpath, "//iframe")[:id]) do
            # 27/10/2021 - Insere o número do contrato no campo da tela para consultar o mesmo e 
            # clica no botão Pesquisar
            find(:xpath, "//tr/td/input[@name='P_CONTRATO']").set($numero_ccb)
            find(:xpath, "//tr/td/input[@name='P_BOTAO']").click    

            # 27/10/2021 - Clica no botão em forma de impressora exibido na linha dos dados co contrato
            find(:xpath, "/html/body/form[6]/table[4]/tbody/tr/td[5]/a").click

            # 27/10/2021 - Clica no radio box para selecionar as opções da tela e clica no botão Imprime
            find(:xpath, "//input[@value='N']").click
            find(:xpath, "//input[@value='T']").click
            find(:xpath, "//input[@id='btImprime']").click
        
            sleep(5)
            # 27/10/2021 - Coloca o foco na nova tela que exibe a CCB e tira foto da tela
            omnifacil.foca_tela
            @texto = '21_CCB - Impressão'
            tirar_foto(indice.to_s + '_' + arquivo + '_' + agente.to_s + '_' + @texto)
        end
    end

end