### Em 25/11/2021 Foi feito um Backup onde este arquivo está com as características originais
### para ser chamado diretamente pelo main1.rb. Caso necessite é só colocar ele de volta no projeto
class Aprovasuper
    def aprovar_supervisor(dados, omnifacil, metodos, i, agente, numero_proposta, sql, indice, arquivo, nova_senha, st_senha)
        # 03/11/2021 Nova Implementação:
        #   Lê uma nova planilha do excel onde constam os logins e senhas dos usuários de mesa, de 
        #   Liberação de Pagamento, Visualização de CCB, etc.
        #   ii = a 3, pega o login de usuário com perfil de Supervisor de Mesa de Crédito
        ii = 3   # Em 05/11/2021 Alterar para 2, para pegar o Usuário de Pagamento na Planilha
        planilha_usu = dados.ler_planilha_usu
        login = planilha_usu[ii][0]
        senha = planilha_usu[ii][1]

        @cod_mesa_proposta = sql.localizar_codigo_mesa(numero_proposta.to_i)
        #@cod_mesa_proposta[["22", "MESA POP", "3", "1", "1", "2", "1", "S"]]

        # Em 05/11/2021 Alterar o conteúdo abaixo de "S" para "L" para testar o usuário de Pagamento
        snova_usu = "S"  # Indica que é usuário Supervisor da Mesa de Crédito
        #binding.pry
        metodos.acessar_url_omnifacil(login, senha, nova_senha, st_senha, i, dados, agente, snova_usu, ii)

        # Chama método que clica no botão que acessa a Mesa de Crédito - Nova Fila
        # PARA AQUI... CHECAR DADOS
        #binding.pry
        omnifacil.selecionar_menu_inicial("MESA DE CRÉDITO - (Nova Fila) ")   
        #
        # 26/08/2021 Busca o Código da Mesa onde está a Proposta por Valmir Soares 
        omnifacil.acessar_grupo_proposta_mesa(@cod_mesa_proposta) 
        omnifacil.foca_tela
        # 16/08/2021 Localiza a Proposta na Mesa onde ela está por Valmir Soares
        omnifacil.localizar_acessar_proposta_mesa(numero_proposta)
        sleep(3)
        #foca_tela
        # 16/08/2021 Fecha popup de Alerta exibido na tela da Proposta por Valmir Soares
        omnifacil.pop_up_alerta_mesa
        sleep(3)

        # FICHA/VERIFICAÇÕES - BOTÃO ACEITAR
        # 16/08/2021 Estando na Ficha/Verificações clica no botão Aceitar por Valmir Soares
        omnifacil.aceitar_etapa_mesa
        # 16/08/2021 Atribúi o nome da aba da tela para validar o botão Aceitar por Valmir Soares

        # DECISÃO - BOTÃO ACEITAR
        @abasmesa = 'Decisão'
        # 16/08/2021 Clica no botão Decisão por Valmir Soares
        omnifacil.clica_botao_decisao(@abasmesa)
        omnifacil.pop_up_alerta_mesa
        omnifacil.foca_tela
        omnifacil.clica_botao_aprovar_decisao
        #binding.pry
        omnifacil.preencher_check_list
        #binding.pry
        omnifacil.finaliza_aprovacao_supervisor(numero_proposta, indice, arquivo, agente)
        omnifacil.foca_tela

    end
end