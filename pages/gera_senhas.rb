require_relative "../support/env.rb"
require_relative "util.rb"
require_relative "omnifacil.rb"
require_relative "metodos.rb"
require_relative "gera_dados.rb"

class GerarSenhas < Util

    def opcoes
        decisao = "N"
        while decisao == "N"
            begin
                #t = 5
                #repete = t.times{print "====="}
                #puts repete
                puts "====================================================="
                puts ">>>>> ESCOLHA A OPÇÃO DESEJADA >>>>>\n"
                puts "====================================================="
                puts "---o Para Gerar Nova Senha, Digite 1: "
                puts "---\n"
                puts "---o Para Gerar Novo Telefone: Celular(2) ou Fixo(3): "
                opc = gets.to_i
                puts "\n"

                if opc == 1
                    nova_senha
                elsif opc == 2
                    puts "=======================================\n"
                    puts "####>>>>> GERADOR DE TELEFONE <<<<<####\n"
                    puts "=======================================\n"
                    puts ">>> Digite um DDD: "
                    ddd = gets.to_i
                    telefone = 'celular'
                    gerar_num_fone(telefone)
                    fone = $numero
                    puts "=== Novo Número Gerado com Sucesso ===\n"
                    puts ">>>>"
                    puts "####>>>> (#{ddd}) #{fone} <<<<<####"
                    puts ">>>>"
                    opc = ""
                elsif opc == 3
                    puts "=======================================\n"
                    puts "####>>>>> GERADOR DE TELEFONE <<<<<####\n"
                    puts "=======================================\n"
                    puts ">>> Digite um DDD: "
                    ddd = gets.to_i
                    telefone = 'fixo'
                    gerar_num_fone(telefone)
                    fone = $numero
                    puts "=== Novo Número Gerado com Sucesso ===\n"
                    puts ">>>>"
                    puts "####>>>> (#{ddd}) #{fone} <<<<<####"
                    puts ">>>>"
                    opc = ""
                elsif opc != 1 or 2 or 3
                    puts "### VOCÊ DIGITOU UMA OPÇÃO INVÁLIDA. FAVOR CORRIGIR"
                    opc = ""
                end
            rescue => exception
                puts "Opção escolhida está errada !!!"
            end

            puts "\n"
            puts ">> Desejeja Encerrar? (S = Sim | N = Não)"
            decisao = gets
            decisao = decisao.upcase[0,1] 

        end
        puts "\n"
        puts ">> Processo Finalizado com sucesso!! \n"
    end

    # Gerar Nova Senha
    def nova_senha
        
        puts "=======================================\n"
        puts "####>>>>> GERADOR DE SENHA <<<<<#### \n"
        puts "=======================================\n"
        puts ">> Digite o Tamnho da Senha: "
        s = gets

        gerar_senhanova(s)
        senha = $senhanova

        puts "=== Nova Senha Gerada com Sucesso ===\n"
        puts ">>>>"
        puts "####>>>> #{senha} <<<<<####"
        puts ">>>>"
        puts "=====================================\n"
    end

    def gerar_senhanova(s)
        s1 = s.to_i
        $senhanova = s1.times.map { [*'0'..'9', *'A'..'Z'].sample }.join
        return $senhanova
    end

    def gerar_num_fone(telefone)
        if telefone.downcase == 'celular'
            $numero = '9' + gerar_numero(1111,9999).to_s + '-' + gerar_numero(1111,9999).to_s
        else
            $numero = gerar_numero(1111,9999).to_s + '-' + gerar_numero(1111,9999).to_s
        end
        return $numero
    end

end