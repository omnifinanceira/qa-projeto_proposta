class Pagamento
    def liberar_pagamento(dados, omnifacil, metodos, i, agente, numero_proposta, sql, indice, arquivo, nova_senha, st_senha)
        #@dados = Gera_dados.new
        #@omnifacil = OmniFacil.new
        #@metodos = Metodos.new   

        # 01/11/2021 - Nova implementação. Pegando dados de usuário nova planilha de usuários
        # ii = a 2, pega dados da linha 2 da planilha Massa_usuarios.xlsx - Usuário de Pagamento/Visualização
        ii = 2
        planilha_usu = dados.ler_planilha_usu
        login = planilha_usu[ii][0]
        senha = planilha_usu[ii][1]

        #planilha = dados.ler_planilha

        #login = planilha[i][19]
        #senha = planilha[i][20]
        
        snova_usu = "L" # Indica que é usuário de Liberação de Pagamento
        ######## --------- Fluxo para Liberação de Pagamento ---------- ########
        #visit "https://hml-omnifacil2.omni.com.br/hmgprojetosint/pck_login.prc_login"
        ##set_url "/pck_login.prc_login"
#
        #metodos.login(login, senha)

        #binding.pry
        metodos.acessar_url_omnifacil(login, senha, nova_senha, st_senha, i, dados, agente, snova_usu, ii)

        metodos.clica_botao_novo_menu(agente)

        metodos.seleciona_agente(agente)
  
        # 25/08/2021 por Valmir Soares
        # Chama o método que clica nos botões do Novo Menu, 
        # neste caso, selecionando a Tela do Simulador
        metodos.clica_botoes_menu(@opc_menu = " OPERACIONAL", @opc_sub_menu1 = "CRÉDITO", @opc_sub_menu2 = "Fila")

        omnifacil.consultar_ficha($numero_proposta)

        within_frame(find(:xpath, "//iframe")[:id]) do
            #binding.pry
            find("select[name='P_FAVORECIDO']").find("option[value='3']").select_option
            vlr_falta_liberar = find(:xpath, "//input[@id='fFaltaLiberar']").value
            #vlr_total_liberar = find(:xpath, "//input[@id='fTotalLiberar']").value
            find(:xpath, "//input[@name='P_VALOR_FAVOREC']").set(vlr_falta_liberar)
            find(:xpath, "//input[@name='P_USUARIO_LIBERA']").set(login)
            find(:xpath, "//input[@name='P_SENHA_LIBERA']").set(senha)

            #page_anterior = page.driver.browser.window_handles.first

            find(:xpath, "//input[@value='Validar']").click
            page.accept_alert
            sleep(5)          
            #page_ultima = page.driver.browser.window_handles.last
            #page_anterior = page.driver.browser.window_handles.first
            #page.driver.browser.switch_to.window(page_ultima)
            #comando = "[onclick="if (ve_motivos_ok(this.form,1,2017,0,0,0,'CDC').submit();"]"
            #page.execute_script("$(#{comando}).click()")
            #onclick="if (ve_motivos_ok(this.form,1,2017,0,0,0,'CDC')) document.form1.submit();"
        end
    end

    def gerada_ccb(i, indice, arquivo, agente, dados)
        within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[@value='Continuar']").click
            sleep(3)
            @texto = '20_CCB - Contrato'
            tirar_foto(indice.to_s + '_' + arquivo + '_' + agente.to_s + '_' + @texto)
            # Código pega o numero do contrato da posição 0 até 15 e joga na variável
            $numero_ccb = find(:xpath, "/html/body/form/center/font").text.gsub(/[^0-9]/,'')[0,15]
            #$numero_contrato = $numero_ccb[0,15]
            dados.gravar_numero_ccb(i, $numero_ccb)
            #find(:xpath, "//input[@value='Voltar']").click

            #visualiza_ccb($numero_ccb)
        end
    end

    #def visualiza_ccb(numero_ccb)
    #    binding.pry
    #    find(:xpath, "//li/a[text()=' OPERACIONAL']").click
    #    #find(:xpath, "//*[@id="navbar-collapse-1"]/ul[1]/li[2]/a"
    #    find(:xpath, "//ul/li/a[text()='FORMALIZAÇÃO']").click
    #    find(:xpath, "//ul/li/a[text()='Impressos']").click
    #    find(:xpath, "//ul/li/a[text()='Contratos']").click
#
    #end

end