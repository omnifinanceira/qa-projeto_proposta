class Queries < SQLConnect
  def pegar_cpf
    lista_cpf = select("
      SELECT a.cpf_cgc
            FROM clientes_proposta  a,
                 retorno_neuro_server b
            WHERE a.proposta = b.proposta
            AND pontuacao > 95
            AND TRUNC(data_execucao) BETWEEN '01-jan-2019'
            AND Trunc(SYSDATE)")
    return lista_cpf.shuffle.first
  end

  def pegar_veiculo(produto)
    veiculo = select("
      select decode(gv.ESPECIE, 1, 'VL', 4, 'VP', 5, 'MOTO', '???') TIPO, ESPECIE, MARCA_MODELO, CAST(ANO_MODELO as varchar(10)), CAST(MODELO as varchar(10)), CAST(MARCA as varchar(10)), CAST(COD_VERSAO as varchar(10)), gv.PLACA, gv.UF_PLACA, gv.UF_LICENCIAMENTO,  CAST(gv.TP_COMBUST as varchar(2))
        from GARANTIAS_VEICULOS gv
        where gv.ANO_MODELO      >= 2015
         and length(gv.PLACA)    = 7
         and length(gv.UF_PLACA) = 2
         and not exists (select 1 from PRESTACOES pr where pr.CONTRATO = gv.CONTRATO and pr.LIQUIDACAO is null)
         and gv.ESPECIE          = #{produto.to_i}")
    return veiculo.shuffle.first
  end

  def pegar_placa(veiculo = "leve", renavam = false)
    #VEÍCULO LEVE
    #MOTO
    #VEÍCULO PESADO
    @veiculo = veiculo
    if (@veiculo.downcase.include?("leve"))
      @veiculo = "VEÍCULO LEVE"
    elsif (@veiculo.downcase.include?("moto"))
      @veiculo = "MOTO"
    else
      @veiculo = "VEÍCULO PESADO"
    end

    if renavam
      renavam = "AND gp.cod_renavam is not null"
    end

    dados_veiculo = select("SELECT gp.placa
            FROM garantias_veiculos_proposta gp,
            proposta p,
            operacoes op
            WHERE gp.proposta = p.proposta
            AND p.operacao = op.codigo
            AND op.grupo2 = '#{@veiculo}'
            AND p.emissao >= trunc(SYSDATE - 30) --ULTIMOS 30 DIAS
            AND gp.placa is not null
            AND gp.chassi is not null")

    return dados_veiculo.shuffle.first
  end

  # 12/08/2021 - BUSCA PROPOSTA NA MESA
  def localizar_codigo_mesa(numero_proposta)
    #binding.pry
    codigo_mesa_proposta = select("select CAST(sc.codigo AS VARCHAR(3)) as codigo,
        CAST(sc.descricao AS VARCHAR(40)) as descricao,
        CAST(sc.mesa_credito AS VARCHAR(2)) as mesa_credito,
        CAST(sc.tipo_fila AS VARCHAR(2)) as tipo_fila,
        CAST(sc.ordem AS VARCHAR(2)) as ordem,
        CAST(sc.imagem AS VARCHAR(2)) as imagem,
        CAST(sc.status AS VARCHAR(2)) as status,
        CAST(fp.pendencia_aprovacao AS VARCHAR(1)) as pendencia_aprovacao
        FROM fila_proposta fp, sc_fila_mesa_credito sc
        WHERE proposta =  #{numero_proposta}
        AND fp.fila_mesa_credito = sc.codigo")
  
    return codigo_mesa_proposta
  end

end
