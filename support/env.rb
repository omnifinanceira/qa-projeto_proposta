require_relative "../pages/gera_dados.rb"
require_relative "../pages/proposta.rb"
require_relative "../pages/metodos.rb"
require_relative "../pages/util.rb"
require_relative "../pages/aprovasuper.rb"
require_relative "../pages/gera_senhas.rb"
require_relative "../support/DAO/SQLConnect.rb"
require_relative "../support/DAO/queries.rb"
require 'capybara/rspec/matchers'
require "oci8"
require "capybara/dsl"
require "selenium-webdriver"
require "pry"
require "simple_xlsx_reader"
require "rubyXL"
require "roo"
require "write_xlsx"

CONFIG = YAML.load_file(File.dirname(__FILE__) + "/ambientes/homolog.yml")
BANCO = YAML.load_file(File.dirname(__FILE__) + "/DAO/config.yml")

include Helper
include Capybara::DSL

Capybara.register_driver :chrome do |app|
  caps = Selenium::WebDriver::Remote::Capabilities.chrome('goog:chromeOptions' => { args: ['start-maximized']})
  driver = Capybara::Selenium::Driver.new(app, {:browser => :chrome, :desired_capabilities => caps})
end

Capybara.configure do |config|
  Capybara.default_driver = :chrome
  config.app_host = CONFIG['url_padrao']
  config.default_max_wait_time = 5
end